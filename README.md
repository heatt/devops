# Clean Code Syllabus

[DevOps Training](https://evosoft-hu.code.siemens.io/devops/) Project is GitLab [Pages](https://evosoft-hu.code.siemens.io/devops/) project that generates static web content.
The content serves as **training material**.

If you require something please leave an [Issue](https://code.siemens.com/evosoft.hu/devops/issues)

# Technical Details

Static web content generator used: [mkdocs](http://mkdocs.org).
Publisher and Content Provider: GitLab [Pages](https://code.siemens.com/siemens/code/blob/master/docs/cd/pages.md).
